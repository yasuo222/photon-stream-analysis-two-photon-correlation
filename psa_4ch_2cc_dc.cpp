/* This program analize photon stream data.
 *
 *   File name : psa_td-4ch.cpp
 *     Version : base old version is "test007a.c".
 *
 *   Created on: 2012/12/11
 *  Last update: 2013/01/08
 *       Author: Y.Y.
 *
 *  Note: This is CPP ver.
 *  This program create time tagged data of coincidence counts and single counts.
 *  
 */


#include <fstream>  //For file I/O.
#include <string>   //For controlling strings.
#include <iostream>
#include <cmath>    //For "abs()" etc.

#define DATASIZE (1000000) // in 4 bytes.
#define CH_A1 (13)		// detection channel # iHR 6m delay
#define CH_A2 ( 3)	 	// detection channel # iHR 10m delay
#define CH_B1 (20)		// detection channel # acton 2m delay
#define CH_B2 ( 6)	 	// detection channel # acton 8m delay

#define TIME_AREA (2000) // in TPB. Calculation area of the X-correlation (x.c.)
#define TIME_CC (20)
#define TIME_0A1 (122) //time of (delta_ta1 = 20 ns)
#define TIME_0A2 (234) //time of (delta_ta1 = 40 ns)
#define TIME_0B1 (0) //time of (delta_tb1 = 0 ns)
#define TIME_0B2 (177) //time of (delta_tb2 = 30 ns)

using namespace std;

bool argcheck(int argc);
bool filecheck(string fnamein);
bool fscheck(fstream &f);

bool argcheck(int argc){
  if(argc == 1){
    cerr << "Error: No argv." << endl;
    return false;
  }
  else 
    return true;
}

bool filecheck(string fnamein){
  if(fnamein.empty()){
    cerr << "Error: File name is empty." << endl;
    return false;
  }
  else if(fnamein.length() < 5){
    cerr << "Error: File name length is too short." << endl;
    return false;
  }
  else if(fnamein.substr(fnamein.length()-4) != ".spc"){
    cerr << "Error: File extension is valid. \"spc\" is expected." << endl;
    return false;
  }
  else {
  return true;
  }
}

bool fscheck(fstream &f){
  if(!f){
    cerr << "Error: file cannot be opened" << endl;
    return false;
  }
  else
    return true;
}


int main(int argc, char** argv)
{
  //Check argument state.
  if(!argcheck(argc))
    return -1;

  string fnamein(argv[1]);
  string fnamein_path = fnamein;

  //Remove directory path from file argument.
  if(fnamein.find("/") != string::npos)
    fnamein = fnamein.substr(fnamein.find_last_of("/")+1);

  if(!filecheck(fnamein))
    return -1;


  //Define file names.
  string fname_core = fnamein.substr(0,fnamein.length()-4);
  string fname_a1 = fname_core + "_a1.d";
  string fname_b1 = fname_core + "_b1.d";
  string fname_a2 = fname_core + "_a2.d";
  string fname_b2 = fname_core + "_b2.d";

  //string fname_a1b1 = fname_core + "_a1b1.d";
  //string fname_a1a2 = fname_core + "_a1a2.d";
  //string fname_a1b2 = fname_core + "_a1b2.d";
  //string fname_a2b1 = fname_core + "_b1a2.d";
  //string fname_b1b2 = fname_core + "_b1b2.d";
  //string fname_a2b2 = fname_core + "_a2b2.d";

  string fname_a1b1_dt = fname_core + "dc_a1b1_dt.d";
  string fname_a1a2_dt = fname_core + "dc_a1a2_dt.d";
  string fname_a1b2_dt = fname_core + "dc_a1b2_dt.d";
  string fname_a2b1_dt = fname_core + "dc_a2b1_dt.d";
  string fname_b1b2_dt = fname_core + "dc_b1b2_dt.d";
  string fname_a2b2_dt = fname_core + "dc_a2b2_dt.d";

  cout << "Input filename: " << fnamein << endl;
  
  //------------------------------------------------------------------------

  fstream fin(fnamein_path.c_str(), std::ios::in|std::ios::binary);

  fstream fout64_a1(fname_a1.c_str(), std::ios::out|std::ios::binary);
  fstream fout64_b1(fname_b1.c_str(), std::ios::out|std::ios::binary);
  fstream fout64_a2(fname_a2.c_str(), std::ios::out|std::ios::binary);
  fstream fout64_b2(fname_b2.c_str(), std::ios::out|std::ios::binary);

  //fstream fout64_a1b1(fname_a1b1.c_str(), std::ios::out|std::ios::binary);
  //fstream fout64_a1a2(fname_a1a2.c_str(), std::ios::out|std::ios::binary);
  //fstream fout64_a1b2(fname_a1b2.c_str(), std::ios::out|std::ios::binary);
  //fstream fout64_a2b1(fname_a2b1.c_str(), std::ios::out|std::ios::binary);
  //fstream fout64_b1b2(fname_b1b2.c_str(), std::ios::out|std::ios::binary);
  //fstream fout64_a2b2(fname_a2b2.c_str(), std::ios::out|std::ios::binary);

  fstream fout_a1b1_dt(fname_a1b1_dt.c_str(), std::ios::out|std::ios::binary);
  fstream fout_a1a2_dt(fname_a1a2_dt.c_str(), std::ios::out|std::ios::binary);
  fstream fout_a1b2_dt(fname_a1b2_dt.c_str(), std::ios::out|std::ios::binary);
  fstream fout_a2b1_dt(fname_a2b1_dt.c_str(), std::ios::out|std::ios::binary);
  fstream fout_b1b2_dt(fname_b1b2_dt.c_str(), std::ios::out|std::ios::binary);
  fstream fout_a2b2_dt(fname_a2b2_dt.c_str(), std::ios::out|std::ios::binary);

  //File check.
  if(!fscheck(fin))
    return -1;

  if(!fscheck(fout_a1b1_dt))
    return -1;

  if(!fscheck(fout_a1a2_dt))
    return -1;

  if(!fscheck(fout_a1b2_dt))
    return -1;

  if(!fscheck(fout_a2b1_dt))
    return -1;

  if(!fscheck(fout_b1b2_dt))
    return -1;

  if(!fscheck(fout_a2b2_dt))
    return -1;
  
  unsigned int data_int;
  char buf_e[4], id;
  unsigned char buf_ue[4];
  unsigned int buf_int;
  int p,q;

  //---Read header information----------
  if(!fin.read((char *) buf_ue,sizeof(buf_ue))){
      cerr << "Error: NG" <<endl;
      return 1;
  }

  cout <<"Header information: " << hex;
  for(q=0;q<4;q++)
      cout << (int) buf_ue[q] <<".";
  cout << dec << endl;

  int tpb = buf_ue[0] + 0x100*buf_ue[1] + 0x10000*buf_ue[2];

  if(buf_ue[3]==0xc1)
      cout << "OK!!!" << endl;
  cout << "TPB : " << tpb << " fs." << endl;
  cout << "----------------------" << endl;

  //---Read time-tagged data---------------
  unsigned int hpdata, gap, buf_a1_num, buf_b1_num, buf_a2_num, buf_b2_num;
  hpdata = gap = buf_a1_num = buf_b1_num = buf_a2_num = buf_b2_num = 0;

  int i=0, j=0, k=0, m=0;
  int delta_a1b1, delta_a1a2, delta_a1b2, delta_a2b1, delta_b1b2, delta_a2b2;
  unsigned int buf_a1[50000], buf_b1[50000], buf_a2[50000], buf_b2[50000];
  unsigned int channel, lpdata;
  long long buf_ll=0; //64bit integer

  unsigned int count_a1 = 0, count_b1 = 0, count_a2 = 0, count_b2 = 0;
  p = 0;


 // cout << sizeof(delta_ab); //よく分からん。
  while( fin.read((char *) buf_ue,sizeof(buf_ue)) ){

      if((id = buf_ue[3] / 0x40) == 1){ //---higher part---

          hpdata = buf_ue[0] + 0x100*buf_ue[1] + 0x10000*buf_ue[2]
                  + buf_ue[3] - (buf_ue[3] / 0x40)*0x40;

        //  buf_ll = 0x100000000*hpdata; //Substitute hpdata to 64bit-buf at first.

          for(i=0; i<buf_a1_num; i++){
              for(j=0; j<buf_b1_num; j++){
				
		//delta_a1b1 = buf_a1[i] - buf_b1[j];
		delta_a1b1 = (buf_a1[i] - TIME_0A1) - (buf_b1[j] - TIME_0B1); //time shift of A1 to B1
		
		if(delta_a1b1 > TIME_AREA)
		  break;
		
		else if(abs(delta_a1b1) < TIME_AREA)
		  fout_a1b1_dt.write((char *) &delta_a1b1, sizeof(delta_a1b1));
		  
	      }
	  }

	for(i=0; i<buf_a1_num; i++){
              for(j=0; j<buf_a2_num; j++){
				
		//delta_a1a2 = buf_a1[i] - buf_a2[j];
		delta_a1a2 = (buf_a1[i] - TIME_0A1) - (buf_a2[j] - TIME_0A2); //time shift of A1 to A2
		
		if(delta_a1a2 > TIME_AREA)
		  break;
		if(abs(delta_a1a2) < TIME_AREA)
		  fout_a1a2_dt.write((char *) &delta_a1a2, sizeof(delta_a1a2));
		 
	      }
	}
      

	for(i=0; i<buf_a1_num; i++){
              for(j=0; j<buf_b2_num; j++){
				
		//delta_a1b2 = buf_a1[i] - buf_b2[j];
		delta_a1b2 = (buf_a1[i] - TIME_0A1) - (buf_b2[j] - TIME_0B2); //time shift of a1 to b2
		
		if(delta_a1b2 > TIME_AREA)
		  break;
		
		if(abs(delta_a1b2) < TIME_AREA)
		  fout_a1b2_dt.write((char *) &delta_a1b2, sizeof(delta_a1b2));
		 
	      }
	}
	

	for(i=0; i<buf_a2_num; i++){
              for(j=0; j<buf_b1_num; j++){
				
		//delta_a2b1 = buf_a2[i] - buf_b1[j]; 
		delta_a2b1 = (buf_a2[i] - TIME_0A2) - (buf_b1[j] - TIME_0B1); //time shift of A2 to B1
		
		if(delta_a2b1 > TIME_AREA)
		  break;

		if(abs(delta_a2b1) < TIME_AREA)
		  fout_a2b1_dt.write((char *) &delta_a2b1, sizeof(delta_a2b1));
		 
	      }
	}
	 

	for(i=0; i<buf_b1_num; i++){
              for(j=0; j<buf_b2_num; j++){
				
		//delta_b1b2 = buf_b1[i] - buf_b2[j];
		delta_b1b2 = (buf_b1[i] - TIME_0B1) - (buf_b2[j] - TIME_0B2); //time shift of B1 to B2

		if(delta_b1b2 > TIME_AREA)
		  break;
		if(abs(delta_b1b2) < TIME_AREA)
		  fout_b1b2_dt.write((char *) &delta_b1b2, sizeof(delta_b1b2));
		  
	      }
	}
      

	for(i=0; i<buf_a2_num; i++){
              for(j=0; j<buf_b2_num; j++){
				
		//delta_a2b2 = buf_a2[i] - buf_b2[j];
		delta_a2b2 = (buf_a2[i] - TIME_0A2) - (buf_b2[j] - TIME_0B2); //time shift of A2 to B2
		
		if(delta_a2b2 > TIME_AREA)
		  break;

		if(abs(delta_a2b2) < TIME_AREA)
		  fout_a2b2_dt.write((char *) &delta_a2b2, sizeof(delta_a2b2));
		 
	      }
	}
	
	buf_a1_num = buf_b1_num = buf_a2_num = buf_b2_num = 0;  //initialize
	continue;
      }

      //---lower part---
      //    gap = buf[3] / 0x20;
      channel = buf_ue[3] - (buf_ue[3] / 0x20)*0x20;
      lpdata = buf_ue[0] + 0x100*buf_ue[1] + 0x10000*buf_ue[2];

      buf_ll = 0x1000000*(long long)hpdata + lpdata;  //Create 64bit data from hp and lp-data
                                           //lpdata is 24-bit data actually.
      if(channel == CH_A1){
          buf_a1[buf_a1_num] = lpdata;
          buf_a1_num++;
          count_a1++; //counting single count of CH-A1
	  	  fout64_a1.write((char *) &buf_ll, sizeof(buf_ll));
      }
      else if(channel == CH_B1){
          buf_b1[buf_b1_num] = lpdata;
          buf_b1_num++;
          count_b1++; //counting single count of CH-B1
	      fout64_b1.write((char *) &buf_ll, sizeof(buf_ll));
      }
      else if(channel == CH_A2){
          buf_a2[buf_a2_num] = lpdata;
          buf_a2_num++;
          count_a2++; //counting single count of CH-A2
          fout64_a2.write((char *) &buf_ll, sizeof(buf_ll));
      }
      else if(channel == CH_B2){
          buf_b2[buf_b2_num] = lpdata;
          buf_b2_num++;
          count_b2++; //counting single count of CH-B2
          fout64_b2.write((char *) &buf_ll, sizeof(buf_ll));
      }
      else{
          cerr << "Error!!!" << endl;
          return 1;
      }
      p++;
  }
  cout << "(total data : " << p << ")" << endl;
  cout << "Single count of Ch.A1 (" << CH_A1 << ") : " << count_a1 << endl;
  cout << "Single count of Ch.A2 (" << CH_A2 << ") : " << count_a2 << endl;
  cout << "Single count of Ch.B1 (" << CH_B1 << ") : " << count_b1 << endl;
  cout << "Single count of Ch.B2 (" << CH_B2 << ") : " << count_b2 << endl;
  cout << "Time of the last count is " << buf_ll*tpb << " fs" << endl;
  //----------------------------------------------------------------

  fin.close();
  fout64_a1.close();
  fout64_b1.close();
  fout64_a2.close();
  fout64_b2.close();
  //fout64_a1b1.close();
  //fout64_a1a2.close();
  //fout64_a1b2.close();
  //fout64_a2b1.close();
  //fout64_b1b2.close();
  //fout64_a2b2.close();
  fout_a1b1_dt.close();
  fout_a1a2_dt.close();
  fout_a1b2_dt.close();
  fout_a2b1_dt.close();
  fout_b1b2_dt.close();
  fout_a2b2_dt.close();


  return 0;
}
